$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION

var tabs = $("#tabs"),
	styler 	= $(".styler"),
	navBtn  = $("#toggle_nav_btn"),
	navOverlay = $(".nav_overlay");

// if(tabsBox.length){
//   include("js/easyResponsiveTabs.js");
// }

	if(tabs.length){
		include("js/jquery-ui.min.js");
	}
	if(styler.length){
		include("js/formstyler.js");
	}

include("js/maskedInput.js");

include("js/device.min.js");

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  $("#toggle_nav_btn, #nav-icon").on("click", function(){
  	$("body").toggleClass("navTrue");
  })


	/* ------------------------------------------------
	FORMSTYLER START
	------------------------------------------------ */

		if (styler.length){
			styler.styler({
				
			});
		}

	/* ------------------------------------------------
	FORMSTYLER END
	------------------------------------------------ */


	/* ------------------------------------------------
	MASKEDINPUT START
	------------------------------------------------ */

		$(function($){
			$("#flat_numb").mask("999", {placeholder:"---"});
			$("#phone_number").mask("(999)999-99-99");
			$(".date").mask("99.99.9999");
			$("#car_id1").mask("a");
			$("#car_id2").mask("999");
			$("#car_id3").mask("aa");
			$("#car_id4").mask("99");
			// $("#flat_numb, #phone_number").css(color: "#ed6f3b");
		});

	/* ------------------------------------------------
	MASKEDINPUT END
	------------------------------------------------ */


	/* ------------------------------------------------
	FORMSTYLER START
	------------------------------------------------ */

		if (tabs.length){
			tabs.tabs();
		}

	/* ------------------------------------------------
	FORMSTYLER END
	------------------------------------------------ */

	


})